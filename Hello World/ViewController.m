//
//  ViewController.m
//  Hello World
//
//  Created by Vaster on 1/23/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize helloLabel;
@synthesize helloButton;
int imageCount;
NSMutableArray * imagesArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Center the x-coordinate of helloButton
    CGPoint Tcenter = self.view.center;
    Tcenter.y = helloButton.center.y;
    [helloButton setCenter:Tcenter];
    
    //Center the x-coordinate of the helloLabel
    Tcenter = self.view.center;
    Tcenter.y = helloLabel.center.y;
    [helloLabel setCenter:Tcenter];
    imageCount = 0;
    
    UIImage * image1 = [UIImage imageNamed:@"Malon.jpg"];
    UIImage * image2 = [UIImage imageNamed:@"Medli2.jpg"];
    UIImage * image3 = [UIImage imageNamed:@"LinkAndZelda.jpg"];
    UIImage * image4 = [UIImage imageNamed:@"Ganondorf.png"];
    UIImage * image5 = [UIImage imageNamed:@"CastleTown.png"];
    UIImage * image6 = [UIImage imageNamed:@"DeathMountain.jpg"];
    UIImage * image7 = [UIImage imageNamed:@"Hyrule.jpg"];
    UIImage * image8 = [UIImage imageNamed:@"Moon.jpg"];
    
    imagesArray = [[NSMutableArray alloc]initWithObjects:image1,image2,image3,image4, image5,image6,image7,image8,nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changeMessage:(id)sender
{
    /*
    [helloLabel setText:@"Matthew Spagnoli completed the first assignment."];
    [helloLabel sizeToFit];
    helloLabel.textAlignment = NSTextAlignmentCenter;
    */
    
    //center the x-coordinate of helloLabel after it is updated with new text
    CGPoint Tcenter = self.view.center;
    Tcenter.y = helloLabel.center.y;
    [helloLabel setCenter:Tcenter];
    
    if( imageCount <= [imagesArray count]){
       
        
        
        if(imageCount == 0){
            UIImageView *imageview = [[UIImageView alloc]
                                      initWithFrame:CGRectMake(-50, -15, 600, 910)];
            [imageview setImage:[UIImage imageNamed:@"Malon.jpg"]];
            [helloLabel setText:@"Hello Malon!"];
            [imageview setContentMode:UIViewContentModeScaleAspectFit];
            [self.view addSubview:imageview];
        }
        else if(imageCount == 1){
             UIImageView *imageview = [[UIImageView alloc]
                                      initWithFrame:CGRectMake(-50, -5, 550, 1000)];
             [imageview setImage:[UIImage imageNamed:@"Medli2.jpg"]];
             [helloLabel setText:@"Hello Medli!"];
             [imageview setContentMode:UIViewContentModeScaleAspectFit];
             [self.view addSubview:imageview];
        }
        else if(imageCount == 2){
            UIImageView *imageview = [[UIImageView alloc]
                                      initWithFrame:CGRectMake(-50, -350, 500, 1550)];
             [imageview setImage:[UIImage imageNamed:@"LinkAndZelda.jpg"]];
             [helloLabel setText:@"Hello Link and Zelda!"];
             [imageview setContentMode:UIViewContentModeScaleAspectFit];
             [self.view addSubview:imageview];

        }
        else if(imageCount == 3){
            UIImageView *imageview = [[UIImageView alloc]
                                      initWithFrame:CGRectMake(-50, 0, 700, 1000)];
             [imageview setImage:[UIImage imageNamed:@"Ganondorf.png"]];
             [helloLabel setText:@"Hello Ganondorf!"];
             [imageview setContentMode:UIViewContentModeScaleAspectFit];
             [self.view addSubview:imageview];

        }
        else if(imageCount == 4){
            UIImageView *imageview = [[UIImageView alloc]
                                      initWithFrame:CGRectMake(-50, -100, 800, 1000)];
             [imageview setImage:[UIImage imageNamed:@"CastleTown.png"]];
             [helloLabel setText:@"Hello CastleTown!"];
             [imageview setContentMode:UIViewContentModeScaleAspectFit];
             [self.view addSubview:imageview];
        }
        else if(imageCount == 5){
            UIImageView *imageview = [[UIImageView alloc]
                                      initWithFrame:CGRectMake(-50, 0, 600, 1000)];
             [imageview setImage:[UIImage imageNamed:@"DeathMountain.jpg"]];
             [helloLabel setText:@"Hello Death Mountain!"];
             [imageview setContentMode:UIViewContentModeScaleAspectFit];
             [self.view addSubview:imageview];
        }
        else if(imageCount == 6){
            UIImageView *imageview = [[UIImageView alloc]
                                      initWithFrame:CGRectMake(-50, 0, 700, 1000)];
             [imageview setImage:[UIImage imageNamed:@"Hyrule.jpg"]];
             [helloLabel setText:@"Hello Hyrule!"];
             [imageview setContentMode:UIViewContentModeScaleAspectFit];
             [self.view addSubview:imageview];
        }
        else if(imageCount == 7){
            UIImageView *imageview = [[UIImageView alloc]
                                      initWithFrame:CGRectMake(-50, -50, 600, 1000)];
             [imageview setImage:[UIImage imageNamed:@"Moon.jpg"]];
             [helloLabel setText:@"Hello... creepy moon!"];
             [imageview setContentMode:UIViewContentModeScaleAspectFit];
             [self.view addSubview:imageview];
            
        }
        
        else if(imageCount == 8){
            
            UIImageView *imageview = [[UIImageView alloc]
                                      initWithFrame:CGRectMake(0, 0, 600, 1200)];
            [imageview setImage:[UIImage imageNamed:@"0RU2AO.jpg"]];
            [helloLabel setText:@"Hello world! Matthew Spagnoli!"];
            [imageview setContentMode:UIViewContentModeScaleAspectFit];
            [self.view addSubview:imageview];
            
            
        }
        
        
        [self.view bringSubviewToFront: helloLabel];
        [self.view bringSubviewToFront: helloButton];
        
        if(imageCount == 8){
            imageCount = 0;
        }
        else{
            imageCount++;
        }
        
    }
    else{
        imageCount = 0;
    }
}


@end
