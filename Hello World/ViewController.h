//
//  ViewController.h
//  Hello World
//
//  Created by Vaster on 1/23/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@property (nonatomic, strong) IBOutlet UILabel *helloLabel;
@property (nonatomic, strong) IBOutlet UIButton *helloButton;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

-(IBAction)changeMessage:(id)sender;


@end

